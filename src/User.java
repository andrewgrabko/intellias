public class User {
    private int id;
    private String firstName;
    private String lastName;
    private int amountOfMoney;

    public User(int id, String firstName, String lastName, int amountOfMoney) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.amountOfMoney = amountOfMoney;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }

    public void decreaseMoney(int amount ){
        this.amountOfMoney -= amount;
    }
}
