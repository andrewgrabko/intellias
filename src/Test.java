import java.util.*;

public class Test {

    private static final String EXIT_MESSAGE = "To exit enter \"exit\"";
    private static final String EXIT_COMMAND = "exit";

    static ArrayList<Product> products = new ArrayList<>();
    static ArrayList<User> users = new ArrayList<>();
    static HashMap< Integer, ArrayList<Integer>> userOrders = new HashMap<>();

    public static void main(String[] args) {
        initData();
        showMainMenu();
        while (true){
            waitForInput();
        }
    }

    public static User getUserById(int userId) {
        for(User user: users){
            if(user.getId() == userId){
                return user;
            }
        }
        throw new RuntimeException("User not found");
    }
    public static Product getProductById(int productId) {
        for(Product product: products){
            if(product.getId() == productId){
                return product;
            }
        }
        throw new RuntimeException("Product not found");
    }

    public static List<String> getAllProduct(){
        List<String> listOfProd = new ArrayList<>();
        for(Product list: products){
            listOfProd.add(list.getName());
        }
        System.out.println(listOfProd);
        return listOfProd;
    }

    public static List<String> getAllUsers(){
        List<String> listOfUser = new ArrayList<>();
        for(User list: users){
            listOfUser.add(list.getFirstName() + " " + list.getLastName());
        }
        System.out.println(listOfUser);
        return listOfUser;
    }

    public static ArrayList<Integer> getUserProducts(int userId){
        if(userOrders.containsKey(userId)){
            return userOrders.get(userId);
        } else{
            return new ArrayList<>();
        }
    }
    public static void printUserProducts(int userId){
        ArrayList<Integer> userProducts = getUserProducts(userId);
        User user = getUserById(userId);
        System.out.println(user.getFirstName() + " " + user.getLastName() + " bought: ");
        for(Integer productId: userProducts){
            Product product = getProductById(productId);
            System.out.println(product.getName() + "-" + product.getPrice());
        }
    }

    public static void purchaseProduct(int userId, int productId){
        User user = getUserById(userId);
        Product product = getProductById(productId);
        if(user.getAmountOfMoney() >= product.getPrice()){
            System.out.println("Successful purchase");
            user.decreaseMoney(product.getPrice());
            ArrayList<Integer> userProducts =  getUserProducts(userId);
            userProducts.add(productId);
            userOrders.put(userId, userProducts);
            printUserProducts(userId);
        }else{
            throw new RuntimeException("You dont have enough money");
        }
    }

    static void waitForInput() {
        Scanner scanner = new Scanner(System.in);

        if (scanner.hasNext(EXIT_COMMAND)) {
            System.exit(0);
        }

        try {
            switch (scanner.nextInt()) {
                case 0:
                    showMainMenu();
                    break;
                case 1:
                    getAllUsers();
                    break;
                case 2:
                    getAllProduct();
                    break;
                case 3:
                    System.out.println("Enter user Id");
                    printUserProducts(scanner.nextInt());
                    break;
                case 4:
                    System.out.println("Enter user id");
                    int userId = scanner.nextInt();

                    System.out.println("Enter product id");
                    int productId = scanner.nextInt();

                    purchaseProduct(userId, productId);
                    break;
                default:
                    System.out.println("Choose correct menu item number");
            }
        } catch (NoSuchElementException e) {
            System.out.println("Invalid input! Please input correct value!");
        } catch (Exception e) {
            System.out.println("Exception occurred. " + e.getMessage());
            showMainMenu();
        }
    }

    static void showMainMenu() {
        System.out.println("1. Show list of users");
        System.out.println("2. Show list of products");
        System.out.println("3. Show list of users purchased the product");
        System.out.println("4. Purchase product");
        System.out.println("\n");

        System.out.println("Enter the menu item number...");
        System.out.println(EXIT_MESSAGE);
    }



    public static void initData(){
        //products
        products.add(new Product(1,"Protein",150));
        products.add(new Product(2,"Creatine",100));
        products.add(new Product(3,"Gainer",120));
        //users
        users.add(new User(1, "John", "Malkovich", 1000));
        users.add(new User(2, "Tom", "Cruise", 250));
        users.add(new User(3, "Leo", "DiCaprio", 300));
    }


}
